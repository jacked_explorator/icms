# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20190819210055) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "areas", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "card_types", force: :cascade do |t|
    t.string "type_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "categories", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "discount_rates", force: :cascade do |t|
    t.float "discount_rate"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "card_type_id"
    t.bigint "partner_id"
    t.index ["card_type_id"], name: "index_discount_rates_on_card_type_id"
    t.index ["partner_id"], name: "index_discount_rates_on_partner_id"
  end

  create_table "members", force: :cascade do |t|
    t.string "first_name"
    t.string "last_name"
    t.string "phone_number"
    t.string "email_address"
    t.string "physical_address"
    t.string "profile_picture_url"
    t.date "date_of_birth"
    t.string "job_title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "card_type_id"
    t.string "profile_picture_key"
    t.string "register_by"
    t.date "expiration_date"
    t.boolean "never_expire"
    t.string "card_number"
    t.index ["card_type_id"], name: "index_members_on_card_type_id"
  end

  create_table "partners", force: :cascade do |t|
    t.string "name"
    t.string "logo_url"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "staff_id"
    t.string "logo_key"
    t.bigint "category_id"
    t.bigint "area_id"
    t.index ["area_id"], name: "index_partners_on_area_id"
    t.index ["category_id"], name: "index_partners_on_category_id"
    t.index ["staff_id"], name: "index_partners_on_staff_id"
  end

  create_table "staffs", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "admin"
    t.boolean "management_staff"
    t.string "first_name"
    t.string "last_name"
    t.string "phone_number"
    t.index ["email"], name: "index_staffs_on_email", unique: true
    t.index ["reset_password_token"], name: "index_staffs_on_reset_password_token", unique: true
  end

  create_table "visits", force: :cascade do |t|
    t.date "visit_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "member_id"
    t.bigint "partner_id"
    t.float "discount_rate"
    t.index ["member_id"], name: "index_visits_on_member_id"
    t.index ["partner_id"], name: "index_visits_on_partner_id"
  end

  add_foreign_key "discount_rates", "card_types"
  add_foreign_key "discount_rates", "partners"
  add_foreign_key "members", "card_types"
  add_foreign_key "partners", "areas"
  add_foreign_key "partners", "categories"
  add_foreign_key "partners", "staffs"
  add_foreign_key "visits", "members"
  add_foreign_key "visits", "partners"
end
