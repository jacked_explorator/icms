require 'faker'

# Partners
Partner.create(name: "Cool Sushi", logo_url: "https://sampleurl.com")
Partner.create(name: "Coffee Everyday", logo_url: "https://sampleurl.com")
Partner.create(name: "Five Guys", logo_url: "https://sampleurl.com")
Partner.create(name: "Thai Express", logo_url: "https://sampleurl.com")
Partner.create(name: "Night Owl", logo_url: "https://sampleurl.com")
Partner.create(name: "Origin Coffee", logo_url: "https://sampleurl.com")
Partner.create(name: "Costco", logo_url: "https://sampleurl.com")
Partner.create(name: "Walmart", logo_url: "https://sampleurl.com")


# Card types
CardType.create(type_name: "Silver")
CardType.create(type_name: "Gold")
CardType.create(type_name: "Black")

# Discount Rate
DiscountRate.create(card_type_id: 1, partner_id: 1, discount_rate: 0.4)
DiscountRate.create(card_type_id: 1, partner_id: 2, discount_rate: 0.3)
DiscountRate.create(card_type_id: 2, partner_id: 1, discount_rate: 0.2)
DiscountRate.create(card_type_id: 2, partner_id: 2, discount_rate: 0.3)

# Category
Category.create(name: "Coffee shop")
Category.create(name: "Restaurant")
Category.create(name: "Retail")

# Area
Area.create(name: "Uptown")
Area.create(name: "Midtown")
Area.create(name: "Downtown")


# Member
0.upto(100){
  Member.create(
    first_name: Faker::Name.name,
    last_name: Faker::Name.last_name,
    phone_number: Faker::PhoneNumber.cell_phone,
    email_address: Faker::Internet.email,
    physical_address: Faker::Address.full_address,
    profile_picture_url: "https://example.com",
    date_of_birth: Faker::Date.between(50.days.ago, Date.today),
    job_title: Faker::Job.title,
    card_type_id: rand(3),
    card_number: Faker::Number.number(10)
  )
}

# Visit
0.upto(150) {
  Visit.create(
    visit_date: rand(2.years).seconds.ago,
    member_id: rand(Member.count) + 1,
    partner_id: rand(Partner.count) + 1,
    discount_rate: rand(0.1...0.7).round(2)
  )
}

puts "--- Seeded"