class CreateMembers < ActiveRecord::Migration[5.1]
  def change
    create_table :members do |t|
      t.string :name
      t.string :category
      t.string :logo_url

      t.timestamps
    end
  end
end
