class AddMemberToVisit < ActiveRecord::Migration[5.1]
  def change
    add_reference :visits, :member, foreign_key: true
  end
end
