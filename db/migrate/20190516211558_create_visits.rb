class CreateVisits < ActiveRecord::Migration[5.1]
  def change
    create_table :visits do |t|
      t.date :visit_date

      t.timestamps
    end
  end
end
