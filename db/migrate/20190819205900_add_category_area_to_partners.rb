class AddCategoryAreaToPartners < ActiveRecord::Migration[5.1]
  def change
    add_reference :partners, :category, foreign_key: true
    add_reference :partners, :area, foreign_key: true
  end
end
