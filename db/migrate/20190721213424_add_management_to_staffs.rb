class AddManagementToStaffs < ActiveRecord::Migration[5.1]
  def change
    add_column :staffs, :management_staff, :boolean
  end
end
