class ChangeMemberPartnerColumnNames < ActiveRecord::Migration[5.1]
  def change
    rename_column :discount_rates, :member_id, :partner_id
    rename_column :visits, :member_id, :partner_id
  end
end
