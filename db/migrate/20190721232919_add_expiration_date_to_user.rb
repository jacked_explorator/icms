class AddExpirationDateToUser < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :expiration_date, :date
    add_column :users, :never_expire, :boolean
  end
end
