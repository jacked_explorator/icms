class RemoveCategoryColInPartners < ActiveRecord::Migration[5.1]
  def change
    remove_column :partners, :category
  end
end
