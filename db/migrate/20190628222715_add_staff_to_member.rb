class AddStaffToMember < ActiveRecord::Migration[5.1]
  def change
    add_reference :members, :staff, foreign_key: true
  end
end
