class CreateUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :users do |t|
      t.string :first_name
      t.string :last_name
      t.string :phone_number
      t.string :email_address
      t.string :physical_address
      t.string :profile_picture_url
      t.date :date_of_birth
      t.string :job_title

      t.timestamps
    end
  end
end
