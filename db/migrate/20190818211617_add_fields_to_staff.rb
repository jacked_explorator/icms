class AddFieldsToStaff < ActiveRecord::Migration[5.1]
  def change
    add_column :staffs, :first_name, :string
    add_column :staffs, :last_name, :string
    add_column :staffs, :phone_number, :string
  end
end
