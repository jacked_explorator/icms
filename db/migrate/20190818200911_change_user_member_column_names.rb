class ChangeUserMemberColumnNames < ActiveRecord::Migration[5.1]
  def change
    rename_column :visits, :user_id, :member_id
  end
end
