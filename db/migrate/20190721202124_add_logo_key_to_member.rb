class AddLogoKeyToMember < ActiveRecord::Migration[5.1]
  def change
    add_column :members, :logo_key, :string
  end
end
