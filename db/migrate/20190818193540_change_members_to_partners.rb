class ChangeMembersToPartners < ActiveRecord::Migration[5.1]
  def change
    rename_table :members, :partners
  end
end
