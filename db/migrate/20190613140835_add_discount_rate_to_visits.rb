class AddDiscountRateToVisits < ActiveRecord::Migration[5.1]
  def change
    add_column :visits, :discount_rate, :float
  end
end
