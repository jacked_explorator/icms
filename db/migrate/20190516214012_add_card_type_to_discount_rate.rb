class AddCardTypeToDiscountRate < ActiveRecord::Migration[5.1]
  def change
    add_reference :discount_rates, :card_type, foreign_key: true
  end
end
