class AddDiscountRatesToMember < ActiveRecord::Migration[5.1]
  def change
    add_reference :discount_rates, :member, foreign_key: true
  end
end
