class ChangeUsersToMembers < ActiveRecord::Migration[5.1]
  def change
    rename_table :users, :members
  end
end
