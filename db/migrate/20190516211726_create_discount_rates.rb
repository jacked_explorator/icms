class CreateDiscountRates < ActiveRecord::Migration[5.1]
  def change
    create_table :discount_rates do |t|
      t.float :discount_rate

      t.timestamps
    end
  end
end
