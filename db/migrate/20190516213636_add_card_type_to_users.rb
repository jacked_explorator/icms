class AddCardTypeToUsers < ActiveRecord::Migration[5.1]
  def change
    add_reference :users, :card_type, foreign_key: true
  end
end
