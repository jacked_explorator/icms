module ApplicationHelper
  def is_admin?
    if !current_staff || !current_staff.admin?
      respond_to do |format|
        format.html{ redirect_to root_path, alert: 'You are not authorized to view this page' }
        format.json { head :unauthorized }
      end
    end
  end

  def is_admin_or_management_staff?
    if !current_staff || (!current_staff.admin? && !current_staff.management_staff?)
      respond_to do |format|
        format.html{ redirect_to root_path, alert: 'You are not authorized to view this page' }
        format.json { head :unauthorized }
      end
    end
  end
end
