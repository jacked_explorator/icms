json.extract! partner, :id, :name, :category, :logo_url, :created_at, :updated_at
json.url partner_url(partner, format: :json)
