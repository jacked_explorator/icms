json.extract! discount_rate, :id, :discount_rate, :created_at, :updated_at
json.url discount_rate_url(discount_rate, format: :json)
