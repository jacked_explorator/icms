json.extract! card_type, :id, :type_name, :created_at, :updated_at
json.url card_type_url(card_type, format: :json)
