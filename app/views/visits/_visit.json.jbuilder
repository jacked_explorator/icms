json.extract! visit, :id, :visit_date, :created_at, :updated_at
json.url visit_url(visit, format: :json)
