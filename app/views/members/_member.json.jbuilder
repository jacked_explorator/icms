json.extract! member, :id, :first_name, :last_name, :phone_number, :email_address, :physical_address, :profile_picture_url, :date_of_birth, :job_title, :created_at, :updated_at
json.url member_url(member, format: :json)
