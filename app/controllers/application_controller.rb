class ApplicationController < ActionController::Base
  include ApplicationHelper

  protect_from_forgery with: :exception
  # before_action :authenticate_staff!

  def home
    if current_staff.nil?
      redirect_to login_path
    elsif current_staff.admin? || current_staff.management_staff?
      redirect_to partners_path and return
    else
      @partner = Partner.find current_staff.partner.id
      @partner.logo_url = @partner.logo_key.present? ? get_signed_url(@partner.logo_key) : ""
      render 'partners/search'
    end
  end

  def get_signed_url(object_key)
    signer = Aws::S3::Presigner.new(client: S3_CLIENT)

    signer.presigned_url(
      :get_object,
      bucket: ENV['S3_BUCKET'],
      key: object_key
    )
  end

end
