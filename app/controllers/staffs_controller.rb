class StaffsController < ApplicationController
  before_action :is_admin?

  def new
    @staff = Staff.new
  end

  def create
    @staff = Staff.new(staff_params)
    @staff.management_staff = true
    new_password = generate_new_password
    @staff.password = new_password

    respond_to do |format|
      if @staff.save
        format.html { redirect_to staffs_path, notice: "staff was successfully created. Password: #{new_password}" }
      else
        format.html { render :new }
      end
    end
  end

  def index
    @staffs = Staff.where('management_staff = true')
  end

  def destroy
    @staff = Staff.find(params[:id])
    @staff.destroy
    respond_to do |format|
      format.html { redirect_to staffs_url, notice: 'Staff was successfully deleted.' }
      format.json { head :no_content }
    end
  end

  def reset_password
    @staff = Staff.find(params[:id])
    new_password = generate_new_password
    @staff.password = new_password
    @staff.save

    respond_to do |format|
      format.html { redirect_to staffs_path, notice: "New password generated: #{new_password}" }
    end
  end

  private 
  def staff_params
    params.require(:staff).permit(:email, :first_name, :last_name, :phone_number)
  end

  def generate_new_password
    range = [*'0'..'9',*'A'..'Z',*'a'..'z']
    Array.new(8){ range.sample }.join
  end
end
