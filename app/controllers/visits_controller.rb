class VisitsController < ApplicationController
  include ApplicationHelper
  before_action :set_visit, only: [:show, :edit, :update, :destroy]
  before_action :is_admin?

  # GET /visits
  # GET /visits.json
  def index
    puts "*** #{params[:id]}"
    if !params[:id].nil?
      @is_partner_specific_page = true;
      @visits = Visit.where(partner_id: params[:id]).order(visit_date: :desc)
      @paginated_visits = @visits.paginate(:page => params[:page], :per_page => 10)
    else
      @visits = Visit.order(visit_date: :desc)
      @paginated_visits = @visits.paginate(:page => params[:page], :per_page => 10)
    end
  end

  # GET /visits/1
  # GET /visits/1.json
  def show
  end

  # GET /visits/new
  def new
    @visit = Visit.new
  end

  # GET /visits/1/edit
  def edit
    @visit.discount_rate = (@visit.discount_rate * 100).round(2)
  end

  # POST /visits
  # POST /visits.json
  def create
    @visit = Visit.new(visit_params)

    respond_to do |format|
      if @visit.save
        format.html { redirect_to @visit, notice: 'Visit was successfully created.' }
        format.json { render :show, status: :created, location: @visit }
      else
        format.html { render :new }
        format.json { render json: @visit.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /visits/1
  # PATCH/PUT /visits/1.json
  def update
    respond_to do |format|
      if @visit.update(
        "discount_rate"=> visit_params["discount_rate"].to_f / 100,
        "partner_id"=> visit_params["partner_id"],
        "member_id"=> visit_params["member_id"]
      )
        format.html { redirect_to @visit, notice: 'Visit was successfully updated.' }
        format.json { render :show, status: :ok, location: @visit }
      else
        format.html { render :edit }
        format.json { render json: @visit.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /visits/1
  # DELETE /visits/1.json
  def destroy
    @visit.destroy
    respond_to do |format|
      format.html { redirect_to visits_url, notice: 'Visit was successfully deleted.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_visit
      @visit = Visit.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def visit_params
      params.require(:visit).permit(:visit_date, :discount_rate, :partner_id, :member_id)
    end
end
