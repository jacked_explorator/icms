class DiscountRatesController < ApplicationController
  before_action :set_discount_rate, only: [:show, :edit, :update, :destroy]

  # GET /discount_rates
  # GET /discount_rates.json
  def index
    redirect_to root_url
  end

  # GET /discount_rates/1
  # GET /discount_rates/1.json
  def show
    redirect_to partner_path(@discount_rate.partner)
  end

  def get_rate

    if(!params[:partner_id] || !params[:card_number])
      render plain: "Partner ID or Member ID is missing", status: :forbidden
      return
    end
    
    partner_id = params[:partner_id].html_safe
    member_card_number = params[:card_number].html_safe
    searched_member = Member.find_by(card_number: member_card_number)

    if searched_member.nil?
      render json: {} and return
    end

    member_id = searched_member.id

    results = ActiveRecord::Base.connection.execute("
        select discount_rate
        from discount_rates as d
        join partners as m
        on d.partner_id = m.id
        join members as u
        on u.card_type_id = d.card_type_id
        where m.id = #{partner_id} and u.id = #{member_id}
      ")

    # Validation 
    member_search_result = Member.where(id: member_id)
    if !member_search_result.empty?
      member = member_search_result[0]
      if !member.is_expired?
        if results.present?
          discount_rate = results.field_values("discount_rate")[0]

          if member.profile_picture_key.present?
            profile_picture_url = get_signed_url(member.profile_picture_key)
          else
            profile_picture_url = ""
          end

          # Save visit record 
          Visit.create(
            visit_date: Time.now,
            member_id: member.id,
            partner_id: partner_id,
            discount_rate: discount_rate
          )

          # Pass back result
          render json: {
            profile_picture_url: profile_picture_url,
            discount_rate: discount_rate,
            first_name: member.first_name,
            job_title: member.job_title
          } and return
        end
      end
    end
      render json: {}
  end

  # GET /discount_rates/new
  def new
    @discount_rate = DiscountRate.new
  end

  # GET /discount_rates/1/edit
  def edit
    @discount_rate.discount_rate = (@discount_rate.discount_rate * 100).round(2)
  end

  # POST /discount_rates
  # POST /discount_rates.json
  def create
    @discount_rate = DiscountRate.new(discount_rate_params)

    respond_to do |format|
      if @discount_rate.save
        format.html { redirect_to @discount_rate, notice: 'Discount rate was successfully created.' }
        format.json { render :show, status: :created, location: @discount_rate }
      else
        format.html { render :new }
        format.json { render json: @discount_rate.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /discount_rates/1
  # PATCH/PUT /discount_rates/1.json
  def update
    respond_to do |format|
      if @discount_rate.update(
        "discount_rate"=>"#{discount_rate_params["discount_rate"].to_f / 100}", 
        "card_type_id"=> discount_rate_params["card_type_id"], 
        "partner_id"=> discount_rate_params["partner_id"]
      )
        format.html { redirect_to @discount_rate, notice: 'Discount rate was successfully updated.' }
        format.json { render :show, status: :ok, location: @discount_rate }
      else
        format.html { render :edit }
        format.json { render json: @discount_rate.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /discount_rates/1
  # DELETE /discount_rates/1.json
  def destroy
    @discount_rate.destroy
    respond_to do |format|
      format.html { redirect_to discount_rates_url, notice: 'Discount rate was successfully deleted.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_discount_rate
      @discount_rate = DiscountRate.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def discount_rate_params
      params.require(:discount_rate).permit(:discount_rate, :card_typem, :card_type_id, :partner_id)
    end
end
