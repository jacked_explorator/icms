class MembersController < ApplicationController
  include ApplicationHelper

  before_action :set_member, only: [:show, :edit, :update, :destroy]
  before_action :is_admin_or_management_staff?
  before_action :generate_profile_picture_url, only: [:show, :edit]

  # GET /members
  # GET /members.json
  def index
    @members = Member.order(id: :desc).paginate(page: params[:page], per_page: 10)
  end

  # GET /members/1
  # GET /members/1.json
  def show
  end

  def visits
    @member_id = params[:id]
    render json: Partner.find(@member_id).visits
  end

  # GET /members/new
  def new
    @member = Member.new
    generate_s3_file_uploader_params
    @expiration_range_selection_form = true
    @new_page = true
  end

  # GET /members/1/edit
  def edit
    signer = Aws::S3::Presigner.new(client: S3_CLIENT)
    generate_s3_file_uploader_params
  end

  # POST /members
  # POST /members.json
  def create
    @member = Member.new(member_params)
    @member.register_by = current_staff.email

    expiration_range = params[:expiration_range]
    @member.expiration_date = Time.now + expiration_range.to_i.year

    respond_to do |format|
      if @member.save
        format.html { redirect_to @member, notice: 'Member was successfully created.' }
        format.json { render :show, status: :created, location: @member }
      else
        @new_page = true
        format.html { render :new }
        format.json { render json: @member.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /members/1
  # PATCH/PUT /members/1.json
  def update
    respond_to do |format|
      if @member.update(member_params)
        format.html { redirect_to @member, notice: 'Member was successfully updated.' }
        format.json { render :show, status: :ok, location: @member }
      else
        format.html { render :edit }
        format.json { render json: @member.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /members/1
  # DELETE /members/1.json
  def destroy
    @member.destroy
    respond_to do |format|
      format.html { redirect_to members_url, notice: 'Member was successfully deleted.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_member
      @member = Member.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def member_params
      params.require(:member).permit(
        :first_name, :last_name, 
        :phone_number, :email_address, 
        :physical_address, :profile_picture_key, 
        :date_of_birth, :job_title, :card_type_id,
        :never_expire, :expiration_date, :card_number)
    end

    def generate_profile_picture_url
      if @member.profile_picture_key.present?
        @member.profile_picture_url = get_signed_url(@member.profile_picture_key)
      else
        @member.profile_picture_url = ""
      end
    end

    def generate_s3_file_uploader_params
      signer = Aws::S3::Presigner.new(client: S3_CLIENT)
      @file_key = "#{@member.id}-#{SecureRandom.uuid}"
      @url = signer.presigned_url(
        :put_object,
        bucket: ENV['S3_BUCKET'],
        key: @file_key
      )
    end
end
