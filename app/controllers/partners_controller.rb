class PartnersController < ApplicationController
  include ApplicationHelper
  before_action :set_partner, only: [:show, :edit, :update, :destroy]
  before_action :is_admin_or_management_staff?, except: :search
  before_action :populate_unset_discount_rates
  before_action :generate_logo_url, only: [:show, :edit]

  # GET /partners
  # GET /partners.json
  def index
    @partners = Partner.all
    @partners = @partners.map do |partner|
      if partner.logo_key.present?
        partner.logo_url = get_signed_url(partner.logo_key)
      else
        partner.logo_url = ""
      end
      partner
    end
  end

  # GET /partners/1
  # GET /partners/1.json
  def show
    @card_types = CardType.order(id: :asc).all
  end

  def visits
    @partner_id = params[:id]
    render json: Partner.find(@partner_id).visits
  end

  # GET /partners/new
  def new
    @partner = Partner.new
    generate_s3_file_uploader_params
    @new_page = true
  end

  # GET /partners/1/edit
  def edit
    generate_s3_file_uploader_params
  end

  # POST /partners
  # POST /partners.json
  def create
    @partner = Partner.new(partner_params)

    respond_to do |format|
      if @partner.save
        format.html { redirect_to @partner, notice: 'Partner was successfully created.' }
        format.json { render :show, status: :created, location: @partner }
      else
        @new_page = true
        format.html { render :new }
        format.json { render json: @partner.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /partners/1
  # PATCH/PUT /partners/1.json
  def update
    respond_to do |format|
      if @partner.update(partner_params)
        format.html { redirect_to @partner, notice: 'Partner was successfully updated.' }
        format.json { render :show, status: :ok, location: @partner }
      else
        format.html { render :edit }
        format.json { render json: @partner.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /partners/1
  # DELETE /partners/1.json
  def destroy

    if params[:password].present?
      if current_staff.valid_password? params[:password]
        @partner.destroy
        render json: {}, status: :ok and return
      end
    end

    render json: {message: "Invalid password"}, status: :unprocessable_entity and return
  end

  def generate_partner_login_credential
    @partner = Partner.find(params[:id])
    
    begin
    if @partner.staff.nil?
      new_staff = @partner.generate_staff_account
      
      redirect_to( partner_path(@partner), notice: "Login credential generated. Password: #{new_staff[:password]}" )
      return
    end
      
    rescue Exception => e
      redirect_to( partner_path(@partner), alert: "Error: #{e}" )
      return
    end

    redirect_to( partner_path(@partner), notice: "Login credential already exist" )
    return
  end

  def reset_password
    @partner = Partner.find(params[:id])
    new_password = @partner.generate_new_password
    @partner.staff.password = new_password
    @partner.staff.save

    respond_to do |format|
      format.html { redirect_to partner_path(@partner), notice: "New password generated: #{new_password}" }
    end
  end

  def search
    render :search
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_partner
      @partner = Partner.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def partner_params
      params.require(:partner).permit(:name, :category_id, :area_id, :logo_key)
    end

  def populate_unset_discount_rates
    Partner.all.each do |partner|
      CardType.all.each do |card_type|
        if partner.discount_rate_of_card_type(card_type.id).nil?
          partner.discount_rates.create(card_type_id: card_type.id, discount_rate: 0.2)
        end
      end
    end
  end

  def generate_logo_url
    if @partner.logo_key.present?
      @partner.logo_url = get_signed_url(@partner.logo_key)
    else
      @partner.logo_url = ""
    end
  end

  def generate_s3_file_uploader_params
    signer = Aws::S3::Presigner.new(client: S3_CLIENT)
    @file_key = "#{@partner.id}-#{SecureRandom.uuid}"
    @url = signer.presigned_url(
      :put_object,
      bucket: ENV['S3_BUCKET'],
      key: @file_key
    )
  end
end
