class DiscountRate < ApplicationRecord
  belongs_to :card_type
  belongs_to :partner
end
