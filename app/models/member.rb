class Member < ApplicationRecord
  has_many :visits, :dependent => :delete_all
  belongs_to :card_type
  validates :first_name, :last_name, :email_address, :phone_number, presence: true
  validates :card_number, format: { with: /\A\d+\z/, message: "allow integer only" }, uniqueness: true

  def is_expired?
    if self.never_expire? || self.expiration_date == nil
      return false
    else
      self.expiration_date < Time.now
    end
  end
end
