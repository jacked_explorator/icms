class Staff < ApplicationRecord

  belongs_to :partner, optional: true

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  def partner
    Partner.find_by(staff_id: self.id)
  end

  def is_management_staff?
    self.management_staff == true
  end
end
