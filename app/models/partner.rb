class Partner < ApplicationRecord
  has_many :visits, :dependent => :delete_all
  has_many :discount_rates, :dependent => :delete_all
  belongs_to :staff, optional: true
  belongs_to :category, optional: true
  belongs_to :area, optional: true
  
  validates :name,  presence: true
  validate :category_area_combination

  def generate_staff_account
    if self.staff.nil?
      new_staff_email = "#{self.name.parameterize}"

      if unique_staff_email? new_staff_email
        new_staff_email = "#{new_staff_email}_#{SecureRandom.hex(2)}"
      end

      new_password = self.generate_new_password;
      self.staff = Staff.create(email: "#{new_staff_email}@#{ENV['HOST']}", password: new_password, password_confirmation: new_password)
      self.save

      return { staff: self.staff, password: new_password }
    end
  end

  def discount_rate_of_card_type(card_type_id)
    self.discount_rates.where(card_type_id: card_type_id).first
  end

  def has_discount_rate_of_card_type?(card_type_id)
    !self.discount_rates.where(card_type_id: card_type_id).empty?
  end

  def unique_staff_email? email
    !Staff.find_by(email: "#{email}@#{ENV['HOST']}").nil?
  end

  def generate_new_password
    range = [*'0'..'9',*'A'..'Z',*'a'..'z']
    Array.new(8){ range.sample }.join
  end

  def category_area_combination
    if self.category.present? && self.area.present?
      existing_combination_partners = Partner.where("category_id = #{self.category.id} AND area_id = #{self.area.id}")
      
      if existing_combination_partners.empty?
        return true
      end

      if existing_combination_partners.size == 1 && existing_combination_partners.include?(self)
        return true
      end

      self.errors.add(:base, "Category and area combination must be unique.")
      return false
    else
      return true
    end
  end
end
