class CardType < ApplicationRecord
  has_many :members
  has_one :discount_rate
end
