class Visit < ApplicationRecord
  before_validation :record_discount_rate
  validates :discount_rate, presence: true

  belongs_to :partner
  belongs_to :member

  def member
    Member.find(self.member_id)
  end

  private 

  def record_discount_rate
    self.discount_rate = self.partner.discount_rate_of_card_type(
      self.member.card_type_id
    ).discount_rate if self.discount_rate.nil?
  end
end
