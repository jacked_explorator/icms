import React, { Component } from "react";

class InputField extends Component {
  state = {};
  render() {
    return <input className="search-input-id" placeholder={this.props.inputName} onChange={this.props.onChange} type="number"/>;
  }
}

export default InputField;
