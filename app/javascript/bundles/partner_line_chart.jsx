import React from "react";
import { Line } from 'react-chartjs-2';
import { rollup } from 'd3-array';
import { parse, endOfDay, addDays, isWithinRange, format, isBefore, isAfter } from 'date-fns';

export default class PartnerLineChart extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      renderData: {},
      groupByDayVisitMap: null,

      renderLabelArray: [],
      renderVisitNumberArray: [],

      dateRange: "All Time",
      partnerName: null,
    };

    this.getLabelVisitNumberArray = this.getLabelVisitNumberArray.bind(this);
    this.updateRenderData = this.updateRenderData.bind(this);
    this.dateRangeOnSelect = this.dateRangeOnSelect.bind(this);
  }
  
  componentWillReceiveProps(nextProps){
    const rawData = nextProps.data;
    const groupByDayVisitMap = rollup(rawData, entry => entry.length, v => format(parse(v.date), "YYYY-MM-DD")); 
    
    this.setState({
      groupByDayVisitMap: groupByDayVisitMap,
      partnerName: this.state.partnerName === null && rawData[0].partner_name
    }, 
      this.updateRenderData
    );
  }

  updateRenderData(){
    const { dateRange } = this.state;
    const rawData = this.props.data;
    const today = endOfDay(new Date());    

    switch(dateRange){

      case "30 Days":
        this.getLabelVisitNumberArray(2, addDays(today, -30), today);
      break;
      
      case "90 Days":
        this.getLabelVisitNumberArray(2, addDays(today, -90), today);
      break;

      case "1 Year":
        this.getLabelVisitNumberArray(28, addDays(today, -365), today);
      break;

      case "All Time":
        const lastRecordDate = parse(rawData[rawData.length-1].date);
        this.getLabelVisitNumberArray(28, lastRecordDate, today, true);
      break;

      default:
        // Default to a year
        this.getLabelVisitNumberArray(7, addDays(today, -365), today);
      break;
    };
  }

  dateRangeOnSelect(e){
    this.setState({
      dateRange: e.target.name
    }, 
      this.updateRenderData
    );
  }

  getLabelVisitNumberArray(interval, rangeStartDate, rangeEndDate, showYear=false ){
    const { groupByDayVisitMap } = this.state;
    
    // Loop through each day in the range
    let groupByDayVisitArray = [];
    for(let datePointer = endOfDay(new Date()); isAfter(datePointer, rangeStartDate); datePointer = addDays(datePointer, -1)){
      const existVisitDayNumber = groupByDayVisitMap.get(format(datePointer, "YYYY-MM-DD"));
      
      if(existVisitDayNumber === undefined){
        groupByDayVisitArray.push({ date: datePointer, visitNumber: 0});
      }else{
        groupByDayVisitArray.push({ date: datePointer, visitNumber: existVisitDayNumber});
      }
    }
    
    // Finished processing groupByDayVisitArray
    let labelArray = [], visitNumberArray = [];
    
    let startDayPointer = null;
    if(groupByDayVisitArray.length < interval){
      startDayPointer = groupByDayVisitArray[0].date;
    }else{
      startDayPointer = groupByDayVisitArray[interval-1].date;
    }
    let rangeVisitNumber = 0;

    groupByDayVisitArray.forEach( visitDayRecord => {
      if(isBefore(startDayPointer, visitDayRecord.date)){
        rangeVisitNumber += visitDayRecord.visitNumber;
      }else{
        visitNumberArray.push(rangeVisitNumber + visitDayRecord.visitNumber);
        labelArray.push(format(startDayPointer, `${showYear ? "YYYY-" : "" }MMM-DD`));

        rangeVisitNumber = 0;
        startDayPointer = addDays(startDayPointer, -1*interval);
      }
    });

    this.setState({
      renderLabelArray: labelArray.reverse(),
      renderVisitNumberArray: visitNumberArray.reverse()
    });
  }

  render(){
    const { renderLabelArray, renderVisitNumberArray, dateRange, partnerName } = this.state;
    
    const data = {
      labels: renderLabelArray,
      datasets: [
        {
          label: partnerName,
          fill: true,
          lineTension: 0.3,
          backgroundColor: 'rgba(75,192,192,0.4)',
          borderColor: 'rgba(75,192,192,1)',
          borderCapStyle: 'butt',
          borderDash: [],
          borderDashOffset: 0.0,
          borderJoinStyle: 'miter',
          pointBorderColor: 'rgba(75,192,192,1)',
          pointBackgroundColor: '#fff',
          pointBorderWidth: 1,
          pointHoverRadius: 5,
          pointHoverBackgroundColor: 'rgba(75,192,192,1)',
          pointHoverBorderColor: 'rgba(220,220,220,1)',
          pointHoverBorderWidth: 2,
          pointRadius: 2,
          pointHitRadius: 10,
          data: renderVisitNumberArray
        }
      ]
    };

    const DropDownButton = () => (
      <div className="dropdown show">
          <a className="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            {dateRange}
          </a>

          <div className="dropdown-menu" aria-labelledby="dropdownMenuLink">
            <a className="dropdown-item" onClick={this.dateRangeOnSelect} name="All Time">All Time</a>
            <a className="dropdown-item" onClick={this.dateRangeOnSelect} name="30 Days">30 Days</a>
            <a className="dropdown-item" onClick={this.dateRangeOnSelect} name="90 Days">90 Days</a>
            <a className="dropdown-item" onClick={this.dateRangeOnSelect} name="1 Year">1 Year</a>
          </div>
        </div>
    );
    
    return(
      <div>
        <DropDownButton />
        <Line data={data} />
      </div>
    );
  }
}