function onLoad(){
  $('#member_never_expire').change(expirationOnCheck);

  disableSelection($('#member_never_expire').is(':checked'));
}

function expirationOnCheck(e){
  const neverExpireChecked = $(this).is(':checked');
  disableSelection(neverExpireChecked);
}

function disableSelection(mode){
  if(mode){
    $('.expiration_date_select select').attr('disabled', true);
  }else{
    $('.expiration_date_select select').attr('disabled', false);
  }
}

// Render component with data
document.addEventListener('DOMContentLoaded', onLoad)