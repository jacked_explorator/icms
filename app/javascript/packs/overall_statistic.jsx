import React from 'react';
import ReactDOM from 'react-dom';
import { rollups } from 'd3-array';
import { Doughnut } from 'react-chartjs-2';
import randomColor from 'randomcolor';
import PartnerLineChart from '../bundles/partner_line_chart';
import { isAfter ,addDays } from 'date-fns'

class DataChart extends React.Component {

  constructor(props){
    super(props);
    
    this.state = {
      renderDataSet: {},
      dataFilter: 'All Time'
    }

    this.getPartnerFrequencyArray = this.getPartnerFrequencyArray.bind(this);
    this.changeFilter = this.changeFilter.bind(this);
  }

  componentDidMount(){
    const { partnerSpecific, data } = this.props;
    
    this.setState({
      renderDataSet: partnerSpecific ? data : this.getPartnerFrequencyArray()
    });
  }

  changeFilter(e){ 
    this.setState({
      dataFilter: e.target.name
    }, () => this.resetData());
    
  }

  resetData(){
    this.setState({
      renderDataSet: this.getPartnerFrequencyArray()
    });
  }

  getPartnerFrequencyArray = () => {
    const rawData = this.props.data;
    let filteredData = []; //will represent the data pushed to the donut
    
    const { dataFilter } = this.state; //is the current filter, this is changed in this.changeFilter()

    let dateShift = 0; //represents how many days should be included in the filter
    let noShift = false; //an option triggered if you want no filter on your data 

    //based on the datafilter, change how much we set out filter by
    if (dataFilter == "Last 30 Days"){
      dateShift = -30;
    }else if(dataFilter =="Last 90 Days"){
      dateShift = -90;
    }else if(dataFilter =="1 Year"){
      dateShift = -365;
    }else{
      noShift = true; //if no filter is specified, activate no shift and avoid the filter loop
    }

    let today = new Date();
    let filterDate = addDays(today,dateShift);

    if(!noShift){
      for(let i = 0; i<=rawData.length-1;i++){
        let ithDate = new Date(rawData[i].date)
          if(isAfter(ithDate, filterDate)){
            filteredData.push(rawData[i]);
          }else{
            break; // if the ith date is outside the limit, we can break the loop to be more efficient
          } 
      }
    }else{
      filteredData = rawData;
    }

    const categoryVisitMap = rollups(filteredData, entry => entry.length, v => v.partner_category);

    let result = {
      labels: [],
      datasets: [{
        data: [],
        backgroundColor: []
      }]
    };

    categoryVisitMap.forEach(
      category => {
        result.labels.push(category[0]);
        result.datasets[0].data.push(category[1]);
        result.datasets[0].backgroundColor.push(randomColor({luminosity: 'light'}));
      }
    );

    return result;
  }

  render(){
    const { partnerSpecific } = this.props;
    const { renderDataSet, dataFilter } = this.state;  

    return(
      <div id="chart-component">
          <div id="chart">
            {
              partnerSpecific ?
                <PartnerLineChart 
                  data={renderDataSet}
                />  
              :
                <React.Fragment>
                  <ChartFilter changeFilter={this.changeFilter} dateRange={dataFilter}/>
                  <Doughnut 
                    data={renderDataSet}
                  />
                </React.Fragment>
            }
          </div>
      </div>
    )
  }
}

class ChartFilter extends React.Component {

  constructor(props){
    super(props);
  }

  render(){
    return(
      <div id="chart-filter">
        <div className="dropdown show">
          <a className="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            {this.props.dateRange}
          </a>
          <div className="dropdown-menu" aria-labelledby="dropdownMenuLink">
            <a className="dropdown-item" onClick={this.props.changeFilter} name="All Time">All Time</a>
            <a className="dropdown-item" onClick={this.props.changeFilter} name="Last 30 Days">Last 30 Days</a>
            <a className="dropdown-item" onClick={this.props.changeFilter} name="Last 90 Days">Last 90 Days</a>
            <a className="dropdown-item" onClick={this.props.changeFilter} name="1 Year">1 Year</a>
          </div>
        </div>
      </div>
    )
  }
}


// Render component with data
document.addEventListener('DOMContentLoaded', () => {
  const node = document.getElementById('static-data')
  const data = JSON.parse(node.getAttribute('data'))
  
  ReactDOM.render(<DataChart data={JSON.parse(data.visit_data)} partnerSpecific={data.partner_specific || false }/>, node);
})