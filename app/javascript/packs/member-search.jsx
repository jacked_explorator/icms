import React from 'react';
import ReactDOM from 'react-dom';
import Autocomplete from 'react-autocomplete';

class MemberSearch extends React.Component{

  constructor(props){
    super(props);

    this.state={
      searchQuery: ""
    }
  }
  
  render(){
    const allPartners = this.props.data;
    const dropdownItem = (item, isHighlighted) =>
      <div key={item.value} style={{ background: isHighlighted ? 'lightgray' : 'white' }} className="search-dropdown-items">
        {item.name} {item.card_number ? ` - ${item.card_number}` : ""}
      </div>;
    const searchMatcher = (item, value) => {
      const cardNumberMatched = item.card_number !== null ? item.card_number.indexOf(value) > -1 : false;
      const nameMatched = item.name.toLowerCase().indexOf(value.toLowerCase()) > -1 ;
      return nameMatched || cardNumberMatched; 
    };
    const searchOnSelect = (label, item) => window.location.replace(`/members/${item.value}`);
    
    return(
      <div id="search-container">
        <span>Search for Member:  </span>
        <Autocomplete
          getItemValue={(item) => item.name}
          items={allPartners}
          renderItem={dropdownItem}
          value={this.state.searchQuery}
          onChange={(e) => this.setState({ searchQuery: e.target.value })}
          onSelect={searchOnSelect}
          shouldItemRender={searchMatcher}
        />
      </div>
    )
  }
}

// Render component with data
document.addEventListener('DOMContentLoaded', () => {
  const node = document.getElementById('member-search-container')
  const data = JSON.parse(node.getAttribute('data'))

  ReactDOM.render(<MemberSearch data={JSON.parse(data.all_members)} />, node)
})