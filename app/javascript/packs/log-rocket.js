window.LogRocket.init('rslc2k/icms');

const sessionInfoTag = document.querySelector('#session-info');
const memberID = sessionInfoTag.dataset.memberId;
const memberName = sessionInfoTag.dataset.memberName;
const memberEmail = sessionInfoTag.dataset.memberEmail;

window.LogRocket.identify(memberID, {
  name: memberName,
  email: memberEmail,
});