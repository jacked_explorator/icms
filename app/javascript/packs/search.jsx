import React, { Component } from "react";
import ReactDOM from 'react-dom';
import InputField from "../bundles/inputField";
import axios from 'axios';

class SearchPartner extends Component {
  state = {
    loading: false,
    memberName: "",
    memberNumber: "",
    memberTitle: "",
    memberProfilePictureUrl: "",
    discountRate: null,
    errorMessage: "Please enter member card number",
    searchMemberCardNumber: "",
    showResults:false
  };

  confirmResultsOnClick = () =>{
    this.setState({
      showResults:false
    })
  }

  searchOnClick = () => {
    const partnerID = this.props.partner.id;
    const memberCardNumber = this.state.searchMemberCardNumber;
    
    if(memberCardNumber.length === 0){ 
      this.setState({
        errorMessage: "Please enter member card number"
      })
      return 
    }

    this.setState({
      loading: true,
      errorMessage: null
    })

    axios.get(`/discount_rate?partner_id=${partnerID}&card_number=${memberCardNumber}`)
      .then((response) => {
        const data = response.data;
        
        if(data.discount_rate === undefined){
          this.setState({
            errorMessage: "Member not found",
            showResults: true
          });
        } else {
          this.setState({
            memberProfilePictureUrl: data.profile_picture_url,
            discountRate: `${data.discount_rate * 100} %`,
            memberName: data.first_name,
            memberTitle: data.job_title,
            showResults: true
          })
        }
        this.setState({loading: false});
      })
      .catch((error) => {  
        this.setState({
          loading: false,
          errorMessage: "Error Occurred. Please try again later",
          showResults: true
        })
      });
  }

  render() {
    const { partner } = this.props;
    const { loading, 
            memberName, 
            memberNumber, 
            memberTitle, 
            memberProfilePictureUrl,
            discountRate, 
            errorMessage, 
            showResults 
          } = this.state;
    
    return (
      <div className="partner-search-container">
        {!showResults &&
        <div>
        {
          partner.logo_url !== "" ?
          <div className="logo-container">
            <img src={partner.logo_url} />
          </div>
          :
          <h2 id="partner-info">{partner.name}</h2>
        }
        {
          partner ?
          <div className="search-input">
            <InputField inputName={"Card number"} onChange={(e) => this.setState({ searchMemberCardNumber: e.target.value })}/>
            <span className="submit-button btn search-button" onClick={this.searchOnClick} >Submit</span>
          </div>
          :
          <span>Please Sign in before action</span>
        }
        </div>
        }
        {showResults &&
        <div className="search-result">
          
        {loading ? "Loading..." :      
            <div>
              {errorMessage ?
                <div className="search-error">
                  <p id="search-result-error">
                    {errorMessage}
                  </p>
                  <span className="submit-button btn search-button" onClick={this.confirmResultsOnClick} >Back</span>
                </div>
              :
                <div className="search-success">
                  <img className="search-profile-pic" src={ memberProfilePictureUrl ? memberProfilePictureUrl : "https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973461_960_720.png"}></img>

                  <p className="search-result-p" id="search-result-name">
                    {`${memberName}`}
                  </p>
                  <p className="search-result-p">
                  {memberNumber}
                  </p>
                  <p className="search-result-p">
                  {memberTitle}
                  </p>
                  <p className="search-result-p" id="search-result-discount">
                  {`${discountRate} Discount Rate`}
                  </p>
                  <span className="submit-button btn search-button" onClick={this.confirmResultsOnClick} >Confirm</span>
                </div>
              }
              </div>
        }
        
        </div>  
        }
      </div>
    );
  }
}

// Render component with data
document.addEventListener('DOMContentLoaded', () => {
  const node = document.getElementById('partner-search')
  const data = JSON.parse(node.getAttribute('data'))
  
  ReactDOM.render(<SearchPartner partner={data.partner} />, node)
})
