import React from 'react';
import ReactDOM from 'react-dom';

class ImageUploader extends React.Component{
  
  constructor(props){
    super(props);

    this.state={
      searchQuery: "",
      uploading: null,
      uploadSuccess: false,
      selectedFile: false
    }

    this.uploadOnClick = this.uploadOnClick.bind(this);
    this.getButtonText = this.getButtonText.bind(this);
    this.fileOnSelected = this.fileOnSelected.bind(this);
  }

  uploadOnClick(e){
    e.preventDefault();

    const { preSignUrl } = this.props;

    var theFormFile = $('#fileInput').get()[0].files[0];

    $.ajax({
      type: 'PUT',
      url: preSignUrl,
      contentType: 'binary/octet-stream',
      processData: false,
      data: theFormFile,
      success: () => { this.setState({uploading: false, uploadSuccess: true}); alert("Profile picture uploaded. Please Submit this page to save changes") },
      error: () => this.setState({uploading: false, uploadSuccess: false})
    });

    this.setState({uploading: true})
  }

  fileOnSelected(e){
    const fileSize = $('#fileInput').get()[0].files[0].size; // In Byte

    if(fileSize <= 20000000){
      this.setState({selectedFile: true});
    }else{
      alert("Upload image must below 20 megabyte")
    }
  }
  
  getButtonText(){
    const { uploading, uploadSuccess } = this.state;

    if(uploading === null){
      return "Upload";
    }

    if(uploading === true){
      return "Uploading ..."
    }

    if(uploadSuccess){
      return "Uploaded";
    }else{
      return "Error";
    }
  }

  render(){
    const { uploading, uploadSuccess, selectedFile } = this.state;
    const { fileKey, formName, objectClassName } = this.props;

    return(
      <div className="file-uploader-container">
        <input 
          id="fileInput" 
          type="file"
          accept="image/*"
          onChange={this.fileOnSelected}
        /> 
        <input type="button" 
          className={`btn btn-outline-primary ${uploading !== null ? uploadSuccess ? "btn-outline-success" : "btn-outline-danger" : "" }`}
          onClick={this.uploadOnClick} 
          value={this.getButtonText()}
          disabled={!selectedFile ? true : false}
        />

        {/* Insert hidden field to save url into database */}
        <input type="text" value={uploadSuccess ? fileKey : ""} name={uploadSuccess ? `${objectClassName}[${formName}]` : ""} hidden={true} readOnly={true}/>
      </div>
    )
  }
}

// Render component with data
document.addEventListener('DOMContentLoaded', () => {
  const node = document.getElementById('image-uploader')
  const data = JSON.parse(node.getAttribute('data'))

  ReactDOM.render(<ImageUploader preSignUrl={data.pre_sign_url} fileKey={data.file_key} formName={data.form_name} objectClassName={data.class_name} />, node)
})