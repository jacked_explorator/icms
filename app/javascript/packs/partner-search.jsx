import React from 'react';
import ReactDOM from 'react-dom';
import Autocomplete from 'react-autocomplete';

class PartnerSearch extends React.Component{

  constructor(props){
    super(props);

    this.state={
      searchQuery: ""
    }
  }
  
  render(){
    const allPartners = this.props.data;
    const dropdownItem = (item, isHighlighted) =>
      <div key={item.value} style={{ background: isHighlighted ? 'lightgray' : 'white' }} className="search-dropdown-items">
        {item.label}
      </div>;
    const searchMatcher = (item, value) => item.label.toLowerCase().indexOf(value.toLowerCase()) > -1;
    const searchOnSelect = (label, item) => window.location.replace(`/visits?id=${item.value}`);
    return(
      <div id="search-container">
        <span>Search for Partner:  </span>
        <Autocomplete
          getItemValue={(item) => item.label}
          items={allPartners}
          renderItem={dropdownItem}
          value={this.state.searchQuery}
          onChange={(e) => this.setState({ searchQuery: e.target.value })}
          onSelect={searchOnSelect}
          shouldItemRender={searchMatcher}
        />
      </div>
    )
  }
}

// Render component with data
document.addEventListener('DOMContentLoaded', () => {
  const node = document.getElementById('partner-search-container')
  const data = JSON.parse(node.getAttribute('data'))

  ReactDOM.render(<PartnerSearch data={JSON.parse(data.all_partner)} />, node)
})