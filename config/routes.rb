Rails.application.routes.draw do

  devise_for :staffs
  resources :staffs, except: :show
  resources :card_types
  resources :discount_rates
  resources :partners
  resources :visits
  resources :members
  resources :categories
  resources :areas

  devise_scope :staff do
    get '/sign-in' => "devise/sessions#new", :as => :login
  end

  get '/generate_partner_login_credential', to: 'partners#generate_partner_login_credential', as: 'new_credential_for_partner'
  get '/reset_password', to: 'partners#reset_password', as: 'reset_password'

  get '/discount_rate', to: 'discount_rates#get_rate'

  get '/partners/:id/search', to: 'partners#search', as: 'partner_search'

  get '/staff/reset_password', to: 'staffs#reset_password', as: 'staff_reset_password'
  post '/staff_registration', to: 'staffs#create', as: 'create_staff'

  root to: 'application#home'
end
