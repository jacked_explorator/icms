Aws.config.update({
  credentials: Aws::Credentials.new(ENV['S3_ACCESS_ID'], ENV['S3_ACCESS_SECRET'])
})

S3_CLIENT = Aws::S3::Client.new(region:ENV['S3_BUCKET_REGION'])