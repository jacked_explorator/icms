# Repo for ICMS (IIC Card Management System)

# Technology stack

1. Built with Ruby on Rails as backend, server only for API usages only
2. Front end will be written with ReactJS, handling View & Controller functionalities.
3. This application is utilizing Docker infrastructure to simplify deployment and development environment setup.

# Development Instruction

1. Install Docker and docker-compose on your machine
2. Run `docker-compose up` at the root level of this folder

# API documentation

Below is the require parameters to fire API calls

## Create

### Create member

```
    {
      "first_name"=>"Oscar",
      "last_name"=>"Wuu",
      "phone_number"=>"121231231231",
      "email_address"=>"sample@gmail.com",
      "physical_address"=>"Somewhere on Earth",
      "profile_picture_url"=>"https://somewhereonearth.com",
      "job_title"=>"Management",
      "card_type_id"=>"3",
      "date_of_birth(1i)"=>"2014",
      "date_of_birth(2i)"=>"8",
      "date_of_birth(3i)"=>"10"
    }
```

### Create partner

```
    {
      "id": 3,
      "name": "Tim Horton",
      "category": "coffee shop",
      "logo_url": "https://logourl.com"
    }
```

## Get

### All partners

GET `/partners.json`

### All members

GET `/members.json`

### All visits

GET `/visits.json`

### All visits of a partner

GET `/partner/visit/<Partner ID>`

### All visits of an member

GET `/member/visit/<Member ID>`

### Discount of an member with a partner

GET `/discount_rate?partner_id=<Partner ID>&member_id=<Member ID>`

## Testing admin account at Staging

email: `test@explorator.ca`
password: `testing`
